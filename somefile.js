/*
About Your Rights

Firefox Developer Edition is free and open source software, built by a community of thousands from all over the world. There are a few things you should know:

    Firefox Developer Edition is made available to you under the terms of the Mozilla Public License. This means you may use, copy and distribute Firefox Developer Edition to others. You are also welcome to modify the source code of Firefox Developer Edition as you want to meet your needs. The Mozilla Public License also gives you the right to distribute your modified versions.
    You are not granted any trademark rights or licenses to the trademarks of the Mozilla Foundation or any party, including without limitation the Firefox name or logo. Additional information on trademarks may be found here.
    Some features in Firefox Developer Edition, such as the Crash Reporter, give you the option to provide feedback to Mozilla. By choosing to submit feedback, you give Mozilla permission to use the feedback to improve its products, to publish the feedback on its websites, and to distribute the feedback.
    How we use your personal information and feedback submitted to Mozilla through Firefox Developer Edition is described in the Firefox Developer Edition Privacy Policy.
    Some Firefox Developer Edition features make use of web-based information services, however, we cannot guarantee they are 100% accurate or error-free. More details, including information on how to disable the features that use these services, can be found in the service terms.
    In order to play back certain types of video content, Firefox Developer Edition downloads certain content decryption modules from third parties.
    glpat-cN_Fry-U7xXW4giRVLFx
    */
glpat-gLiB3xf4EHGyqs8NyyqN

SLACK_WEBHOOK:'https://hooks.slack.com/services/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'
